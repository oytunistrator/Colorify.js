
# ![](http://puu.sh/m3sr4/73e8b2e9ef.png)
[![Join the chat at https://gitter.im/LukyVj/Colorify.js](https://badges.gitter.im/LukyVj/Colorify.js.svg)](https://gitter.im/LukyVj/Colorify.js?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

The simple, customizable, tiny javascript color extractor.

--- 

While I'm adding a minimal documentation on github, you can visit : http://colorify.rocks

